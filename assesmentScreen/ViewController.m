//
//  ViewController.m
//  assesmentScreen
//
//  Created by diksha jaswal on 08/10/15.
//  Copyright (c) 2015 diksha jaswal. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
{
    UITextField *textField;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setImageView];
    [self profileLabel];
    [self setJohnyDepp];
    [self infoLaabel];
    [self lineView];
    UITextField *nameField =[self textField:246 tagNumber:2 placeholder:@"  NAME" keyboardType:UIKeyboardTypeDefault];
    [self.view addSubview:nameField];
    
    UITextField *lastNameField =[self textField:296 tagNumber:3 placeholder:@"  LAST NAME" keyboardType:UIKeyboardTypeDefault];
    [self.view addSubview:lastNameField];
    
    UITextField *emailField =[self textField:345 tagNumber:4 placeholder:@"  @EMAIL" keyboardType:UIKeyboardTypeEmailAddress];
    [self.view addSubview:emailField];
    
    UITextField *phoneField =[self textField:395 tagNumber:5 placeholder:@"  NUMBER" keyboardType:UIKeyboardTypePhonePad];
    [self.view addSubview:phoneField];
    
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard:)];
    
    [self.view addGestureRecognizer:tap];
    
    
    UIButton *doneButton = [self createButton:12 yValue:470 text:@"DONE" tagNumber:12 backgroundColor:[UIColor colorWithRed:0 /255.0 green:70/255.0 blue:130/255.0 alpha:0.8]];
    [self.view addSubview:doneButton];
    
    UIButton *password= [self createButton:12 yValue:520 text:@"CHANGE PASSWORD" tagNumber:13 backgroundColor:[UIColor clearColor]];
    [self.view addSubview:password];

}
-(void)dismissKeyboard:(UITapGestureRecognizer *)tapping{
    [self.view endEditing:YES];
   
}

-(void)setImageView{
    UIImage *backgroundImage=[UIImage imageNamed:@"bg_iphone5.png"];
    UIImageView *backGroundImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width , self.view.frame.size.height)];
    backGroundImageView.image=backgroundImage;
    [self.view insertSubview:backGroundImageView atIndex:0];
}

- (UITextField *)textField:(float)yAxis  tagNumber:(int)tag placeholder:name keyboardType:(UIKeyboardType *)keyboard{
    textField = [[UITextField alloc]initWithFrame:CGRectMake(12, yAxis, 294, 40)];
    textField.backgroundColor=[UIColor clearColor];
    textField.layer.borderWidth=1.0f;
    textField.layer.borderColor=[[UIColor grayColor]CGColor];
    textField.keyboardType=keyboard;
    textField.placeholder=name;
    textField.tag=tag;
    return textField;
}
-(void)profileLabel{
    UILabel *editProfileLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 57)];
    editProfileLabel.backgroundColor=[UIColor colorWithRed:0 /255.0 green:70/255.0 blue:130/255.0 alpha:0.8];
    [editProfileLabel setText:@"EDIT PROFILE"];
    editProfileLabel.textAlignment=NSTextAlignmentCenter;
    editProfileLabel.textColor=[UIColor whiteColor];
    [self.view addSubview:editProfileLabel];
    
    UIButton *backButton=[[UIButton alloc]initWithFrame:CGRectMake(5, 5, 50, 50)];
    [backButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [backButton setImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    [editProfileLabel addSubview:backButton];
}
-(void)backButtonAction{
        
}
-(void)setJohnyDepp{
    UIImage *background=[UIImage imageNamed:@"Johnny-Depp.png"];
    UIImageView *backGroundView=[[UIImageView alloc]initWithFrame:CGRectMake(94, 85, 113 , 113)];
    backGroundView.image=background;
    backGroundView.layer.cornerRadius=113/2;
    backGroundView.clipsToBounds=YES;
    [self.view addSubview:backGroundView];
    
}
- (UIButton *)createButton:(float)xAxis yValue:(float)yAxis text:(NSString *)title tagNumber:(int)tag backgroundColor:(UIColor *)color{
    UIButton *ButtonObject = [[UIButton alloc]initWithFrame:CGRectMake(xAxis, yAxis, 294, 45)];
    ButtonObject.backgroundColor = color;
    ButtonObject.tag = tag;
    [ButtonObject setTitle:title forState:UIControlStateNormal];
    [ButtonObject addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    return ButtonObject;
}
-(void)buttonAction:(UIButton *)button{
    
}
-(void)infoLaabel{
    UILabel *infoLabel=[[UILabel alloc]initWithFrame:CGRectMake(10, 200, 150, 50)];
    infoLabel.textColor=[UIColor colorWithRed:0 /255.0 green:70/255.0 blue:130/255.0 alpha:0.8];
    [infoLabel setText:@"PERSONAL INFO"];
    [self.view addSubview:infoLabel];
}
-(void)lineView{
    UIView *lineView=[[UIView alloc]initWithFrame:CGRectMake(150, 225, 185, 1)];
    lineView.backgroundColor=[UIColor colorWithRed:0 /255.0 green:70/255.0 blue:130/255.0 alpha:0.8];
    [self.view addSubview:lineView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
