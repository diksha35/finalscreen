//
//  AppDelegate.h
//  assesmentScreen
//
//  Created by diksha jaswal on 08/10/15.
//  Copyright (c) 2015 diksha jaswal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

